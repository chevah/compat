# Copyright (c) 2013 Adi Roiban.
# See LICENSE for details.
"""
Constants for the compatibility layer.
"""
WINDOWS_PRIMARY_GROUP = u'Users'
DEFAULT_FILE_MODE = 0666
DEFAULT_FOLDER_MODE = 0777
